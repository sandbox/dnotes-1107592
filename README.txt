This module provides VERY basic display capability for google calendars, using
a CCK field for data storage.  To use the module, install it as usual and make
a CCK textarea field named google_calendar.  Attached to any node type, that
field will display the google calendar pasted into the field on the node form.